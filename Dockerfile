FROM node:14.15.1-alpine3.11 as build-deps
WORKDIR /usr/src/app
COPY . ./

RUN npm rebuild node-sass
RUN npm install
RUN npm run build

FROM nginx:1.18.0-alpine
COPY --from=build-deps /usr/src/app/build /usr/share/nginx/html
COPY ./nginx/nginx.conf /etc/nginx/conf.d/default.conf