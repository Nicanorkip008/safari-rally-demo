import React from 'react'


import './navbar.scss'
import { Link } from '@material-ui/core'

function NavBar(props) {
  return (
    <header className="navbar" id="sticky-navbar">
      <div className={`navbar-inner ${props.cname}`} style={{ paddingLeft: '6rem' }}>
        <Link href="#"  style={{ color: 'black' }}>
         <h1>Safari Rally 2021</h1>
        </Link>
      </div>
    </header>
  )
}

export default NavBar
