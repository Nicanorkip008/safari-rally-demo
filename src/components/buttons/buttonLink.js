import { Link, makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles(() => ({
  registerButton: {
    backgroundColor: '#8d00ff',
    color: 'white',
    padding: '0.5rem 1.7rem',
    borderRadius: '4px',
    fontSize: '14px',
    fontWeight: 'bold',
    textTransform: 'uppercase',
    '&:hover': {
      textDecoration: 'none',
      color: '#8d00ff',
      backgroundColor: 'white',
    },
  },
}))

function ButtonLink({ buttonText, url, style }) {
  const classes = useStyles()
  return (
    <Link
      href={url}
      className={`${classes.registerButton} button`}
      className={classes.registerButton}
      style={style}
      target="_blank"
      rel="noopener noreferrer"
    >
      {buttonText}
    </Link>
  )
}

export default ButtonLink
