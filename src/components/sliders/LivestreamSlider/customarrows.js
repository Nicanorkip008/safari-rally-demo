import * as React from 'react'
import arrow from '../../../assets/images/arrow.svg'

const CustomLeftArrow = ({ onClick }) => (
  <i onClick={() => onClick()} className="custom-left-arrow">
    <img src={arrow} className=""></img>
  </i>
)
const CustomRightArrow = ({ onClick }) => {
  return (
    <i className="custom-right-arrow" onClick={() => onClick()}>
      <img src={arrow} className=""></img>
    </i>
  )
}
export { CustomLeftArrow, CustomRightArrow }
