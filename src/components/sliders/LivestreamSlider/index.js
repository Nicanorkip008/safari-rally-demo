import React, { useMemo } from 'react'
import Carousel from 'react-multi-carousel'
import { makeStyles } from '@material-ui/core/styles'
import './index.scss'
import { CustomLeftArrow, CustomRightArrow } from './customarrows'
import LazyLoadMyImage from '../../lazyload'
import { Link } from 'react-router-dom'

const useStyles = makeStyles((theme) => ({
  title: {
    fontSize: '41px',
    fontWeight: 'bold',
    lineHeight: 1.09,
    letterSpacing: 'normal',
    marginBottom: '10px',
    color: ' #ffffff',
    [theme.breakpoints.down('sm')]: {
      fontSize: '28px',
      marginLeft: '20px',
    },
  },
}))


const LiveStreamSlider = ({ slides, category }) => {
  const classes = useStyles()
  const { carouselOptions, slidesToShow } = useMemo(() => {
    const slidesToShow = slides.map((item, index) => {
      return (
        <div key={index}>
          {item.enableExternalLink ? (
            <a key={index} href={item.externalLink} target="_blank">
              <LazyLoadMyImage src={item.featuredImage[0] && item.featuredImage[0].url} />
            </a>
          ) : (
            <Link
              key={index}
              to={{
                pathname: `/${item.category}/${item.id}`,
                state: { id: item.id },
              }}
            >
              <LazyLoadMyImage src={item.featuredImage[0] && item.featuredImage[0].url} />
            </Link>
          )}
          <div className="livestreamslider__bottom-wrapper">
            <div className="livestreamslider__bottom-title">
              <h3>{item.name}</h3>
              <p>{item.shortDescription}</p>
            </div>
          </div>
        </div>
      )
    })

    const carouselOptions = {
      containerClass: 'livestreamslider__contents',
      arrows: true,
      swipeable: true,
      draggable: true,
      autoplaySpeed: 4000,

      ssr: true,
      deviceType: 'mobile',

      responsive: {
        desktop: {
          breakpoint: { min: 992, max: 8000 },
          items: 3,
          paritialVisibilityGutter: 60,
        },
        tablet: {
          breakpoint: { min: 768, max: 992 },
          items: 2,
          partialVisibilityGutter: 60,
        },
        mobile: {
          breakpoint: { min: 0, max: 768 },
          items: 1,
          partialVisibilityGutter: 0,
        },
      },
    }

    return {
      slidesToShow,
      carouselOptions,
    }
  }, [slides])

  return (
    <>
      <div className="livestreamslider__section">
        <h2 className={classes.title}>{category}</h2>
        <Carousel
          partialVisible={true}
          customRightArrow={<CustomRightArrow />}
          customLeftArrow={<CustomLeftArrow />}
          {...carouselOptions}
        >
          {slidesToShow}
        </Carousel>
      </div>
    </>
  )
}

export default LiveStreamSlider
