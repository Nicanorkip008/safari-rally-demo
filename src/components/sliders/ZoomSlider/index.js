import React, { useMemo } from 'react'
import Carousel from 'react-multi-carousel'
import { makeStyles } from '@material-ui/core/styles'
import './index.scss'
import JoinButton from '../../JoinButton/buttonLink'
import { CustomLeftArrow, CustomRightArrow } from '../LivestreamSlider/customarrows'
import bgzoom from '../../../assets/images/bg-zoom.png'
import LazyLoadMyImage from '../../lazyload'
import zoomplaceholder from '../../../assets/images/zoomplaceholder.jpg'
const useStyles = makeStyles((theme) => ({
  root: {
    backgroundImage: `url(${bgzoom})`,
    backgroundPosition: 'center',
    backgroundSize: '130vw 100%',
    paddingBottom: '100px',
    paddingTop: '100px',
    [theme.breakpoints.down('sm')]: {
      paddingBottom: '50px',
      paddingTop: '50px',
    },
  },
  zooms: {
    marginLeft: '10%',
    [theme.breakpoints.down('sm')]: {
      marginLeft: '0',
    },
  },
  title: {
    fontSize: '41px',
    fontWeight: 'bold',
    lineHeight: 1.09,
    letterSpacing: 'normal',
    color: '#ffffff',
    margin: '0',
    [theme.breakpoints.down('sm')]: {
      fontSize: '28px',
      marginLeft: '20px',
    },
  },
}))

const ZoomSlider = ({ slides }) => {
  const classes = useStyles()
  const { carouselOptions, slidesToShow } = useMemo(() => {
    const slidesToShow = slides.map((item, index) => {
      return (
        <>
          <LazyLoadMyImage
            src={item.image[0] !== undefined ? item.image[0].url : zoomplaceholder}
          />
          <div className="zoomslider__bottom">
            <div className="zoomslider__bottom-title">
              <h3>{item.name}</h3>
            </div>
            <div className="zoomslider__button">
              <JoinButton url={item.zoomRoomUrl} id={item.id} buttonText="Join" />
            </div>
          </div>
        </>
      )
    })

    const carouselOptions = {
      containerClass: 'livestreamslider__contents',
      arrows: true,
      swipeable: true,
      draggable: true,
      autoplaySpeed: 4000,
      ssr: true,
      deviceType: 'mobile',

      responsive: {
        desktop: {
          breakpoint: { min: 992, max: 8000 },
          items: 3,
          paritialVisibilityGutter: 60,
        },
        tablet: {
          breakpoint: { min: 768, max: 992 },
          items: 2,
          partialVisibilityGutter: 60,
        },
        mobile: {
          breakpoint: { min: 0, max: 768 },
          items: 1,
          partialVisibilityGutter: 0,
        },
      },
    }

    return {
      slidesToShow,
      carouselOptions,
    }
  }, [slides])
  return (
    <>
      <div className={classes.root}>
        <div className="zoomslider__section">
          <h2 className={classes.title}>Zoom Meetings</h2>
          <Carousel
            partialVisible={true}
            customRightArrow={<CustomRightArrow />}
            customLeftArrow={<CustomLeftArrow />}
            {...carouselOptions}
          >
            {slidesToShow}
          </Carousel>
        </div>
      </div>
    </>
  )
}

export default ZoomSlider
