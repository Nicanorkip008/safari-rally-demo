import { Link, makeStyles } from '@material-ui/core'
import React from 'react'

const useStyles = makeStyles((theme) => ({
  shareButton: {
    color: 'white',
    padding: '0.5rem 1.7rem',
    fontSize: '14px',
    fontWeight: 'bold',
    borderRadius: '4px',
    backgroundColor: '#8d00ff',
    textTransform: 'uppercase',
    '&:hover': {
      textDecoration: 'none',
      color: '#8d00ff',
      backgroundColor: 'white',
    },
    // [theme.breakpoints.down('sm')]: {
    //   display: 'block',
    //   width: '100%',
    // },
  },
}))

function ShareButton({ buttonText, url }) {
  const classes = useStyles()
  return (
    <Link href={url} className={classes.shareButton} target="_blank" rel="noopener noreferrer">
      {buttonText}
    </Link>
  )
}

export default ShareButton
