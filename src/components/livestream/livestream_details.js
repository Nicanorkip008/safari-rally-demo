import { Container, Grid } from '@material-ui/core'
import React from 'react'
import ShareButton from '../shareButton/shareButton'
import './livestream_details.scss'
import DefaultBackgroundImage from '../../assets/images/livestreamBackground.jpg'

import KeyboardBackspaceIcon from '@material-ui/icons/KeyboardBackspace'
import NavBar from '../navbar/navbar'
import { withRouter } from 'react-router-dom'


function LivestreamDetails(props) {
  const timeOffset = 0

  const goback = () => {
    props.history.goBack()
    window.scrollTo(0, 0)
  }

  return (
    <React.Fragment>
          <NavBar />
          <div className="livestreams">
            <div
              className="livestream-background-image"
              style={{
                backgroundImage: `url(${DefaultBackgroundImage})`,
                backgroundSize: 'cover',
                backgroundRepeat: 'no-repeat',
                backgroundPosition: 'center',
              }}
            >
              <div className="test"></div>
              <Container className="livestream__details">
                {/* loader */}

                {/* go back button */}
                <div className="livestream__details_goBack" onMouseDown={(e) => goback(e)}>
                  <KeyboardBackspaceIcon /> <p>Back</p>
                </div>

                {/* livestream image(s) */}
                  <Grid container className="livestream__details_content-block livestream-image">
                    <Grid item xs={12} md={6} className="livestream__details_image-content-grid">
                      <span>
                        <iframe
                          onLoad="this.src += '#!referrer='+encodeURIComponent(location.href)+'&realReferrer='+encodeURIComponent(document.referrer)"
                          src={`${'https://followthebeat.bbvms.com/p/goodbye_20_20_video_speler/c/4087462.html'}?t=${timeOffset}&inheritDimensions=true`}
                          width="100%"
                          height="315"
                          frameBorder="0"
                          webkitallowfullscreen
                          mozallowFullscreen
                          oallowFullScreen
                          msallowfullscreen
                          allowFullScreen
                          allow="autoplay; fullscreen"
                        ></iframe>
                      </span>
                    </Grid>
                    <Grid item xs={12} md={6} className="livestream__details_image-content-grid">
                      <img
                            src="https://res.cloudinary.com/nicanor/image/upload/v1612814490/safari-rally/luc-van-loon-eSRFIbxqV2A-unsplash.jpg"
                            alt="livestream detail"
                            className="livestream__details_image"
                          />
                    </Grid>
                  </Grid>
              </Container>

              {/* details */}
              <div className="Overlay">
                <Container>
                  {/* title */}
                  <div className="livestream-content-header">
                        <h2 className="livestream__details_title">#Event Name</h2>
                  </div>
                  <Grid container className="livestream__details_content-block-details">
                    {/* time */}
                      <Grid item xs={6} md={3}>
                        <span className="content-block-details-label">Tijd</span>
                        <br />
                        <p className="content-block-details-value">
                          08:00
                          -{' '}
                          13:00
                        </p>
                      </Grid>

                    {/* location */}
                      <Grid item xs={6} md={3}>
                        <span className="content-block-details-label">Locatie</span>
                        <br />
                        <p className="content-block-details-value">Naivasha, Kenya</p>
                      </Grid>

                    {/* genre */}
                      <Grid item xs={6} md={3}>
                        <span className="content-block-details-label">Genre</span>
                        <br />
                          <span
                            className="content-block-details-value details-value-genre"
                          >
                            Racing
                          </span>
                      </Grid>

                  </Grid>
                </Container>
              </div>
            </div>

            <Container>
              <Grid container className="livestream__row2_details">
                  <div>
                    <h2 className="content-block-description-title">Description</h2>
                    <hr />
                    <div className="content-block-description-text">
                      <p>
                      Spread the love en like de social pagina van de maker van deze stream. Klik op
                      de like button hieronder.</p>
                    </div>
                  </div>

                {/* share */}
                <div className="livestream__row2_details_share-content">
                  <Grid item xs={12} md={6} className="livestream__row2_details_share-content-text">
                    
                    <ShareButton url="#" buttonText="Like" />
                  </Grid>
                </div>
              </Grid>
            </Container>
          </div>
    </React.Fragment>
  )
}

export default withRouter(LivestreamDetails)
