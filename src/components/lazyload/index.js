import React from 'react'
import { LazyLoadImage, trackWindowScroll } from 'react-lazy-load-image-component'

const LazyLoadMyImage = ({ alt, height, src, width, scrollPosition }) => (
  <div>
    <LazyLoadImage
      alt={alt}
      height="100%"
      delayMethod="throttle"
      delayTime={60}
      // scrollPosition={scrollPosition}
      effect="black-and-white"
      src={src} // use normal <img> attributes as props
      width="100%"
    />
  </div>
)

export default trackWindowScroll(LazyLoadMyImage)
