import React from 'react'
import { Route, Switch, Redirect } from 'react-router-dom'
import withTracker from '../withTracker'
import { routes } from './RoutesConfig'

export const Routes = () => {
  return (
    <Switch>
      {routes.map((route, index) => (
        <Route
          key={index}
          path={route.path}
          exact={route.exact || false}
          component={withTracker(route.component)}
        />
      ))}
      <Route path="*" render={(props) => <Redirect to={{ pathname: '/error' }} />} />
    </Switch>
  )
}
