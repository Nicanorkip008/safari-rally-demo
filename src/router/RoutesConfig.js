import LivestreamDetails from '../components/livestream/livestream_details'
import Home from '../pages'
import Programme from '../pages/Programme'
import LiveStreams from '../pages/livestreams'
import Zoom from '../pages/Zoom'
import NotFound from '../pages/NotFound/'

// Route Views
export const routes = [
  {
    path: '/',
    component: Home,
    exact: true,
  },
  {
    path: '/programma',
    component: Programme,
    exact: true,
  },
  {
    path: '/streams',
    component: LiveStreams,
    exact: true,
  },
  {
    path: '/zoom/:slug',
    component: Zoom,
    exact: true,
  },
  {
    path: '/:category/:slug/:id',
    component: LivestreamDetails,
    exact: true,
  },
  {
    path: '/error',
    component: NotFound,
  },
]
