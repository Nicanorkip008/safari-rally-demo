import React, { Component } from 'react'
import GoogleAnalytics from 'react-ga'

GoogleAnalytics.initialize('UA-162894605-2')

const withTracker = (WrappedComponent, options = {}) => {
  const trackPage = (page) => {
    if (process.env.NODE_ENV !== 'production') {
      return
    }

    GoogleAnalytics.set({
      page,
      ...options,
    })
    GoogleAnalytics.pageview(page)
  }

  const BASENAME = process.env.REACT_APP_BASENAME || ''

  // eslint-disable-next-line
  const HOC = class extends Component {
    componentDidMount() {
      // eslint-disable-next-line
      const page = this.props.location.pathname + this.props.location.search
      trackPage(`${BASENAME}${page}`)

      window.dataLayer.push({
        event: 'PageView',
        page: page,
        url: this.props.location.pathname,
        searchTerm: this.props.location.search,
      })
    }

    componentDidUpdate(prevProps) {
      const currentPage = prevProps.location.pathname + prevProps.location.search
      const nextPage = this.props.location.pathname + this.props.location.search

      if (currentPage !== nextPage) {
        trackPage(`${BASENAME}${nextPage}`)
      }
    }

    render() {
      return <WrappedComponent {...this.props} />
    }
  }

  return HOC
}

export default withTracker
