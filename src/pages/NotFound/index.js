import React from 'react'

import './NotFound.scss'

export default function NotFound({ history }) {
  return (
    <div className="container--error">
      <div id="main">
        <div className="fof">
          <h1>Fout 404</h1>
          <p>
            Pagina niet gevonden.
            <span onMouseDown={() => history.goBack()}>Terug!</span>
          </p>
        </div>
      </div>
    </div>
  )
}
