import React from 'react'
import { Grid, makeStyles } from '@material-ui/core'
import LiveStreamSlider from '../components/sliders/LivestreamSlider'
import livestreambg from '../assets/images/livestreanbg.jpg'
// import Partners from '../components/partners/partners'
import './livestreams.scss'

const useStyles = makeStyles((theme) => ({
  icon: {
    marginRight: theme.spacing(2),
  },
  hero: {
    maxWidth: '100%',
    height: '100vh',
    backgroundPosition: 'center',
    backgroundRepeat: 'no-repeat',
    backgroundSize: 'cover',
  },
  livestreamtop: {
    marginTop: '-50vh',
    [theme.breakpoints.down('sm')]: {
      marginTop: '-35vh',
    },
  },
  livestreams: {
    marginLeft: '10%',
    [theme.breakpoints.down('sm')]: {
      marginLeft: '0',
    },
  },
  heroContent: {
    backgroundColor: theme.palette.background.paper,
    padding: theme.spacing(2, 0, 6),
  },
  heroButtons: {
    marginTop: theme.spacing(4),
  },
  cardGrid: {
    paddingTop: theme.spacing(8),
    paddingBottom: theme.spacing(8),
  },
  card: {
    height: '100%',
    display: 'flex',
    flexDirection: 'column',
  },
  cardMedia: {
    paddingTop: '56.25%', // 16:9
  },
  cardContent: {
    flexGrow: 1,
  },
  footer: {
    padding: theme.spacing(2),
    color: 'white',
  },
  slider: {
    marginTop: '100px',
  },
}))

export const groupedData = {
  0: [
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612813308/safari-rally/lisa-wagstaff-X13hkujF2EY-unsplash.jpg"
    }],
    category: "Accomodation",
    name: "Wag Neil Hotel",
    shortDescription: "Fulfill your desires",
    id: 1
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612813222/safari-rally/edvin-johansson-rlwE8f8anOc-unsplash.jpg"
    }],
    category: "Accomodation",
    name: "Nightly Hot Spurs",
    shortDescription: "Everything Hot, cools life",
    id: 2
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612813220/safari-rally/michiel-annaert-lC-kg3FZ3PM-unsplash.jpg"
    }],
    category: "Accomodation",
    name: "Alfajiri Showers",
    shortDescription: "Live for today, dream for tomorrow",
    id: 1
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612813220/safari-rally/alexander-kaunas-xEaAoizNFV8-unsplash.jpg"
    }],
    category: "Accomodation",
    name: "Wawika Hotels",
    shortDescription: "Everything is bright",
    id: 1
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612813220/safari-rally/johannes-w-IQKuHc2lils-unsplash.jpg"
    }],
    category: "Accomodation",
    name: "New Dawn",
    shortDescription: "Let's watch the sun rise together",
    id: 1
  }
],
1: [
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612813921/safari-rally/george-sultan-csTf8spFGe0-unsplash.jpg"
    }],
    category: "Teams",
    name: "Caltex",
    shortDescription: "Digital Microscopes",
    id: 1
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612814237/safari-rally/edu-galvez-nWP9b6AjA6g-unsplash.jpg"
    }],
    category: "Teams",
    name: "Mentos",
    shortDescription: "Always there to win",
    id: 2
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612814490/safari-rally/luc-van-loon-eSRFIbxqV2A-unsplash.jpg"
    }],
    category: "Teams",
    name: "Oilbya",
    shortDescription: "The World Travellers",
    id: 2
  },
  {
    externalLink: "",
    featuredImage: [{
      url: "https://res.cloudinary.com/nicanor/image/upload/v1612814238/safari-rally/macau-photo-agency-StUByLsBmmo-unsplash.jpg"
    }],
    category: "Teams",
    name: "Naivasha Safaris",
    shortDescription: "The World Travellers",
    id: 3
  },
],
}

export default function HomepageDemo() {
  const classes = useStyles()

  return (
    <React.Fragment>
      <main>
        <div className={classes.hero} style={{ backgroundImage: `url(${livestreambg})` }}>
          <div className="livestreamslider__page-titlewrapper">
            <div className="livestreamslider__page-title">
              <h1>
                SAFARI RALLY <br /> <span style={{fontSize: '160px'}}>2021</span>
              </h1>
            </div>
            <div className="livestreamslider__page-description">
              <p>
                This year, we going bigger and better. We bringing the safari rally event to your finger tips.<br />
                The Safari Rally will be livestreamed with real time updates from our media teams, partners and the organisers.
              </p>
            </div>
          </div>
        </div>
        <Grid className={`${classes.livestreams} ${classes.livestreamtop}`} maxWidth="lg">
          {groupedData && groupedData[0] && (
            <>
              <LiveStreamSlider slides={groupedData[0]} category="Accomodation" />
              <LiveStreamSlider slides={groupedData[1]} category="Teams" />
            </>
          )}
        </Grid>
        {/* <Partners /> */}
      </main>
    </React.Fragment>
  )
}
