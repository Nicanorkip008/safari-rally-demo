import React from 'react'
import { Route, Router } from 'react-router-dom'
import { history } from './router/history'

import { ThemeProvider } from '@material-ui/core/styles'
import CssBaseline from '@material-ui/core/CssBaseline'
import 'react-multi-carousel/lib/styles.css'
import theme from './theme'
import HomepageDemo from './pages'
import livestream_details from './components/livestream/livestream_details'

function App() {
  return (
    <ThemeProvider theme={theme}>
      <Router history={history}>
        <Route exact path="/" component={HomepageDemo} />
        <Route exact path="/:team/:id" component={livestream_details} />
        <CssBaseline />
      </Router>
    </ThemeProvider>
  )
}

export default App
