## Follow The Beat
A new year eve celebration project

## Tools
- React

## Getting Started
```
# clone repository
git clone git@bitbucket.org:incentro-ondemand/follow-the-beat.git

# cd into follow-the-beat
cd follow-the-beat

# to install the dependencies
npm install

# to run the projects locally with live hot reloading
npm run start

# to make a production build and run it locally
npm run build
```

## Firestore Database
Dev Database name is `ftb-dev`. For all `.env` credentials, consult @Nicanor or @Dalton. If you adding new data to the database, make sure to provide all fields listed below.

### Collections
- category
- livestreams

### Fields as per the collection
- category collection fields
    - name(string)
- livestreams collection fields
    - name(string)
    - category(reference)
    - imageUrl(string)
    - livestreamUrl(string)
    - location(string)
    - genre(array)
    - timeStarting(timestamp)
    - timeEnding(timestamp)
    - createdAt(timestamp)
